export default function binarySearch(array, targetValue) {
    let start = 0
    let end = array.length - 1


    while (start <= end) {
        let middle = Math.floor((start + end) / 2)
        let guess = array[middle]

        if (guess === targetValue) {
            return middle
        }

        if (guess > targetValue) {
            end = middle - 1
        }

        if (guess < targetValue) {
            start = middle + 1
        }

        return null
    }
}