class LinkedListNode {
    constructor(value) {
        this.value = value
        this.next = null
    }
}

export default class LinkedList {
    constructor() {
        this.head = null
        this.length = 0
    }

    getHead() {
        return this.head
    }

    size() {
        return this.length
    }

    append(data) {
        const newNode = new LinkedListNode(data)

        if (this.length === 0) {
            this.head = newNode
        } else {

            let current = this.head;

            while (current.next) {
                current = current.next;
            }

            current.next = newNode;

        }
        this.length++
        return this.head
    }

    insert(data, position) {
        position = +position

        let newNode = new LinkedListNode(data)
        let currentNode = this.getHead()

        if (position > this.size()) {
            return null
        }

        if (position === 0) {
            newNode.next = currentNode
            this.head = newNode
            return currentNode
        }

        let index = 0
        while (index < position - 1) {
            currentNode = currentNode.next
            index++
        }

        newNode.next = currentNode.next
        currentNode.next = newNode
        this.length++

        return newNode
    }

    print() {
        let currentNode = this.head
        let printString = ''
        while (currentNode.next != null) {
            printString += `${currentNode.value} --> `
            currentNode = currentNode.next
        }
        printString += `${currentNode.value} --> null`

        return printString
    }
}