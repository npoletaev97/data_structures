export default class BinaryTree {

    constructor() {
        // root of a binary seach tree 
        this.root = null;
    }

    printTree() {

    }

    getRootNode() {
        return this.root;
    }

    findNode(data) {
        if (!this.root) return false

        let current = this.root
        let found = false

        while (current && !found) {
            if (data < current.value) {
                current = current.left
            } else if (data > current.value) {
                current = current.right
            } else {
                found = current
            }
        }

        if (!found) return undefined;

        return found


    }

    findMinNode(node) {
        if (node.left === null)
            return node;
        else
            return this.findMinNode(node.left);
    }

    search(node, data) {
        if (node === null)
            return null;

        else if (data < node.data)
            return this.search(node.left, data);
        else if (data > node.data)
            return this.search(node.right, data);
        else
            return node;
    }

    insert(data) {
        let newNode = new BinaryTreeNode(data)

        if (this.root === null) {
            this.root = newNode
        } else {
            this.insertNode(this.root, newNode)
        }
    }

    insertNode(node, newNode) {
        if (newNode.data < node.data) {
            if (node.left === null) {
                node.left = newNode
            } else {
                this.insertNode(node.left, newNode)
            }
        } else {
            if (node.right === null) {
                node.right = newNode
            } else {
                this.insertNode(node.right, newNode)
            }
        }
    }

    remove(data) {
        this.root = this.removeNode(this.root, data)
    }

    removeNode(node, data) {
        if (node === null) {
            return null
        } else if (data > node.data) {
            node.right = this.removeNode(node.right, data)
            return node
        } else if (data < node.data) {
            node.left = this.removeNode(node.left, data)
            return node
        } else {

            if (node.left === null && node.right === null) {
                node = null
                return node
            }
            if (node.left === null) {
                node = node.right
                return node
            }
            if (node.right === null) {
                node = node.left
                return node
            }

            let minimalNode = this.findMinNode(node.right);
            node.data = minimalNode.data;

            node.right = this.removeNode(node.right, minimalNode.data);
            return node;
        }
    }
}


class BinaryTreeNode {
    constructor(value) {
        this.value = value
        this.left = null
        this.right = null
    }
}