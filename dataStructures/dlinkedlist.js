class DoublyLinkedListNode {
    constructor(value) {
        this.value = value
        this.next = null
        this.previous = null
    }
}

class DoublyLinkedList {
    constructor() {
        this.head = null
        this.tail = null
        this.length = 0
    }

    getHead() {
        return this.head
    }

    getTail() {
        return this.tail
    }

    size() {
        return this.length
    }

    getByIndex(index) {
        if (index < 0 || index >= this.length) return null

        let current = this.head
        for (let i = 0; i < index; i++) {
            current = current.next
        }
        return current
    }

    removeAtIndex(index) {
        if (index === 0) return this.removeHead()

        const prev = this.getByIndex(index - 1)
        if (prev == null) return null

        prev.next = prev.next.next
        this.length--
    }

    removeHead() {
        this.head = this.head.next
        this.length--
    }

    pushBack(data) {
        const newNode = new DoublyLinkedListNode(data)
        if (this.length === 0) {
            this.head = newNode
            this.tail = newNode
        } else {

            let current = this.head;

            while (current.next) {
                current = current.next;
            }

            newNode.previous = current
            current.next = newNode;
            this.tail = newNode

        }
        this.length++
        return this.tail
    }

    pushFront(data) {
        const newNode = new DoublyLinkedListNode(data)

        if (this.length === 0) {
            this.head = newNode
            this.tail = newNode
        } else {
            newNode.next = this.head
            this.head.previous = newNode
            this.head = newNode
        }

        this.length++
        return this.head
    }

    insert(position, data) {
        position = +position

        let newNode = new DoublyLinkedListNode(data)
        let currentNode = this.getHead()

        if (position > this.size() || position < 0) {
            return null
        }

        if (position === 0) {
            newNode.next = currentNode
            this.head = newNode
            this.length++

            return currentNode
        }

        let index = 0
        while (index < position - 1) {
            currentNode = currentNode.next
            index++
        }

        newNode.next = currentNode.next
        currentNode.next = newNode
        this.length++
    }

    clear() {
        if (this.length === 0) {
            return 'Already empty'
        } else {

            let currentNode = this.head

            while (currentNode.next) {
                currentNode.previous = null
                currentNode = currentNode.next
            }

            currentNode = null
            this.tail = null
        }
        this.length = 0
    }

    toArray() {
        if (this.length === 0) {
            return 'Empty'
        }
        let currentNode = this.head
        let array = []
        while (currentNode.next) {
            array.push(currentNode.value)
            currentNode = currentNode.next
        }

        array.push(currentNode.value)

        return array
    }
}

DoublyLinkedList.fromValues = function (...values) {
    const dl = new DoublyLinkedList()
    for (let i = values.length - 1; i >= 0; i--) {
        dl.pushFront(values[i])
    }
    return dl
}


module.exports = DoublyLinkedList