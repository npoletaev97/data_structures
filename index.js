

function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min)) + min; // Максимум не включается, минимум включается
}


function drawTree(height) {
    for (let i = 0; i < height; i++) {
        let star = '*';
        let space = ' ';

        for (let j = 1; j <= i; j++) {
            star = star + '**';
        }

        let spacesBefore = space.repeat(height - i - 1);
        star = spacesBefore + star;
        console.log(star);
    }
}

drawTree(500)
